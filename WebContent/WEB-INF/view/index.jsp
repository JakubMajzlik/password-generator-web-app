<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html class="fill">
<head>
<meta charset="UTF-8">

	<link href='<c:url value="/resources/css/bootstrap.min.css"/>' rel="stylesheet" /> 
	
	<title>Password Generator</title>
	<style type="text/css">
		.fill {
			height: 100%;
		}
	</style>
</head>
<body class="text-center fill">
	<div class="header bg-dark text-white p-1">
		<h1>Password Generator</h1>
	</div>
	
	<br>
	<div class="container fill">
		<div class="row">
			<div class="col-4">
				<form:form action="generate" modelAttribute="generator" >
					Upper case characters <form:checkbox path="useUppers"/> <br>
					Lower case characters <form:checkbox path="useLowers"/> <br>
					Special characters <form:checkbox path="useSpecial"/> <br>
					Numbers <form:checkbox path="useNumbers"/> <br>
					<br>
					Length <form:input path="length" /> <br>
					Number of passwords <form:input path="numberOfPasswords" /> <br>
					<input type="submit" value="Generate"/> <br>
					<br>
					<div class="alert-danger" role="alert">
						<form:errors path="length"/>
					</div>
					<br>
					<div class="alert-danger" role="alert">
						<form:errors path="numberOfPasswords"/>
					</div>
				</form:form>
			</div>
			<div class="col-8">
				<h2>Passwords:</h2>
				<c:if test="${generator.passwordGenerated == true}">
					<c:forEach begin="1" end="${generator.getNumberOfPasswords().intValue() }">
					
						${generator.getPassword() } <br>
						
					</c:forEach>
				</c:if>
			</div>
		</div>			
	</div>
	<div class="footer text-light bg-dark">

	  <div class="footer-copyright text-center py-3">
	  	� 2018 Copyright: <a href="https://gitlab.com/JakubMajzlik">Jakub Majzlik</a>
	  </div>
	
	</div>
</body>
</html>