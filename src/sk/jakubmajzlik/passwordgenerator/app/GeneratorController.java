/**
 * Project: Password Generator Web App
 * Author: Jakub Majzl�k
 * File: GeneratorController.java
 * Date created: 08.05.2018
 */


package sk.jakubmajzlik.passwordgenerator.app;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GeneratorController {
	
	public GeneratorController() {
	}

	@RequestMapping("/")
	public String indexPage(Model model) {
		if(!model.containsAttribute("generator")) {
			model.addAttribute("generator", new Generator());
		}

		return "index";
	}
	
	@RequestMapping("/generate")
	public String generatePassword(
			@ModelAttribute("generator")@Valid Generator generator,
			 BindingResult result) {
		
		//If none of the options is selected, the default one will be used
		if(!generator.isUseLowers() && !generator.isUseUppers() &&
				!generator.isUseNumbers() && !generator.isUseSpecial()) {
			generator.setUseLowers(true);
		}

		//If validation failed, no password will be generated
		if(result.hasErrors()) {
			generator.setPasswordGenerated(false);
		} else {
			generator.setPasswordGenerated(true);
		}
		return "index";
	}
	
}
