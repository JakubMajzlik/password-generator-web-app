/*
 * Project: Password Generator Web App
 * Author: Jakub Majzl�k
 * File: Generator.java
 * Date created: 08.05.2018
 */

package sk.jakubmajzlik.passwordgenerator.app;

import java.util.Random;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class Generator {
	
	private String lowers = "abcdefghijklmnopqrstuvwxyz";
	private String uppers = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private String special = "~!@#$%^&*()_+=-[]{}\';/.,<>";
	private String numbers = "0123456789";
	
	@NotNull(message="The field \"Lengh\" is required")
	@Max(value=64)
	@Min(value=5, 
		message="Minimal length of the password must be at least 5 characters")
	private Integer length = 8;
	
	@NotNull(message="The field \"Number of passwords\" is required")
	@Min(value=1,
		message="You can generate at least 1 password")
	private Integer numberOfPasswords = 1;
	
	private boolean passwordGenerated = false; 
	
	private boolean useLowers = true;
	private boolean useUppers;
	private boolean useNumbers;
	private boolean useSpecial;
	
	public Generator() {
	}
	
	public boolean isPasswordGenerated() {
		return passwordGenerated;
	}

	public void setPasswordGenerated(boolean passwordGenerated) {
		this.passwordGenerated = passwordGenerated;
	}

	public Integer getNumberOfPasswords() {
		return numberOfPasswords;
	}

	public void setNumberOfPasswords(Integer numberOfPasswords) {
		this.numberOfPasswords = numberOfPasswords;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public boolean isUseLowers() {
		return useLowers;
	}

	public void setUseLowers(boolean useLowers) {
		this.useLowers = useLowers;
	}

	public boolean isUseUppers() {
		return useUppers;
	}

	public void setUseUppers(boolean useUppers) {
		this.useUppers = useUppers;
	}

	public boolean isUseNumbers() {
		return useNumbers;
	}

	public void setUseNumbers(boolean useNumbers) {
		this.useNumbers = useNumbers;
	}

	public boolean isUseSpecial() {
		return useSpecial;
	}

	public void setUseSpecial(boolean useSpecial) {
		this.useSpecial= useSpecial;
	}

	public String getPassword() {
		Random random = new Random(System.nanoTime());
		
		StringBuilder stringBuilder = new StringBuilder();
		StringBuilder passwordString = new StringBuilder();
		
		if(useUppers) {
			stringBuilder.append(uppers);
		}
		if(useLowers) {
			stringBuilder.append(lowers);
		}
		if(useSpecial) {
			stringBuilder.append(special);
		}
		if(useNumbers) {
			stringBuilder.append(numbers);
		}
		
		if(stringBuilder.length() == 0) {
			return "";
		}

		String chars = new String(stringBuilder);
		
		for(int i = 0; i < length; i++) {
			passwordString.append(chars.charAt(random.nextInt(chars.length())));
		}
		return new String(passwordString);
	}
	
}
