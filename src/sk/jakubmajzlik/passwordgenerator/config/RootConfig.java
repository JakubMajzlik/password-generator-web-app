/**
 * Project: Password Generator Web App
 * Author: Jakub Majzl�k
 * File: RootConfig.java
 * Date created: 08.09.2018
 */

package sk.jakubmajzlik.passwordgenerator.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("sk.jakubmajzlik.passwordgenerator")
public class RootConfig {

}
